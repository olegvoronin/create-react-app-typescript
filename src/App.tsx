import * as React from 'react';
import './App.css';
import MarketplaceApp from './components/MarketplaceApp';

const MockData = require('./mock.json');

class App extends React.Component {

  public state: IAppState;

  constructor(props: IAppProps) {
    super(props);
    this.state = {
      editing: undefined,
      apps: MockData.apps
    };
  }

  public edit(app: IMarketplaceApp) {
    if (this.state.editing === app.id) {
      this.setState({editing: undefined});
    } else {
      this.setState({editing: app.id});
    }
  }

  public change(app: IMarketplaceApp) {
    this.setState({editing: app.id});
  }

  public addApp() {
    this.setState({apps: this.state.apps.concat([{
      'id': '1',
      'name': 'Skype',
      'category': undefined,
      'description': 'Call your app friends'
    }])});
    // this.setState({apps: []});
  }

  render() {
    return (
      <div className="App">
        <main>
          List of apps! <button onClick={this.addApp.bind(this, this)}>Add new</button>

          <div>{this.state.apps.map((_app: IMarketplaceApp) => 
              <MarketplaceApp
                key={_app.id}
                app={_app}
                onEdit={this.edit.bind(this, _app)}
                editing={this.state.editing === _app.id}
              />
          )}</div>
        </main>
      </div>
    );
  }
}

export default App;