interface IMarketplaceApp {
	id?: string,
	name: string,
	category?: string,
	description?: string,
}

interface IMarketplaceAppProps {
	// key?: string,
	app: IMarketplaceApp,
	editing?: boolean,
	onEdit: () => void,
}

interface IMarketplaceAppState {
	name?: string,
	category?: string,
	description?: string,
}

interface IAppState {
  editing?: string,
  apps: Array<IMarketplaceApp>
}

interface IAppProps  {

}