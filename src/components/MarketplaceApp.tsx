/// <reference path="./../interfaces.d.ts"/>

import * as React from 'react';

class MarketplaceApp extends React.Component<IMarketplaceAppProps, IMarketplaceAppState> {

  public state: IMarketplaceAppState;

  constructor(props: IMarketplaceAppProps) {
    super(props);
    this.state = {
      name: this.props.app.name,
      category: this.props.app.category,
      description: this.props.app.description,
    };
  }

  public handleEdit() {
    this.props.onEdit();
  }

  public handleChange(event: any) {
    console.log(event.currentTarget);
    this.setState({ description : event.currentTarget.value });
  }

  render() {
    return (
      <article
        onDoubleClick={e => this.handleEdit()}
        className="cr-marketplace-app" 
        style={{outline: this.props.editing ? '2px dotted red' : 'none'}}
      >

        {!this.props.editing ? <h2>{this.state.name}</h2> : null}
        {this.props.editing ?
          <input
            type="text" 
            value={this.state.name}
            onChange={e => this.setState({name: e.currentTarget.value})}
          /> : null}

        {!this.props.editing
          ? <h3>[{this.state.category}]</h3>
          : <div>
              <select
                value={this.state.category}
                onChange={e => this.setState({category: e.currentTarget.value})}
              >
                {['misc', 'productivity', 'communications'].map((cat, index) =>
                  <option key={index} value={cat}>{cat}</option>
                )}
                <option value="" selected={true} disabled={true} hidden={true}>--select category--</option>
              </select>
            </div>
        }

        {!this.props.editing ? <p>{this.state.description}</p> : null}
        {this.props.editing ?
          <textarea 
            value={this.state.description}
            onChange={e => this.setState({description: e.currentTarget.value})}
          /> : null}
      </article>
    );
  }
}

export default MarketplaceApp;