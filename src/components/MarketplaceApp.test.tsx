import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as ReactTestUtils from 'react-dom/test-utils';
import MarketplaceApp from './MarketplaceApp';

describe('MarketplaceApp', () => {

	let mockData: IMarketplaceApp = {
      'id': '1',
      'name': 'Skype',
      'category': undefined,
      'description': 'Call your app friends'
    };

	it('renders without crashing', () => {
		const div = document.createElement('div');
		ReactDOM.render(<div />, div);
	});

	it('renders app name', () => {
		const reactComponent = ReactTestUtils.renderIntoDocument(
			<MarketplaceApp
                key={mockData.id}
                app={mockData}
                onEdit={()=>{}}
			/>,
		) as HTMLElement;

    	const reactComponentNode = ReactDOM.findDOMNode(reactComponent);

    	expect(reactComponentNode.textContent).toEqual(
    		expect.stringContaining(mockData.name),
    	);
	});

});